## Version 2.0.0

* rewrite for Puppet 4 compatibility
* change a bunch of parameters to make the module clearer
* add CI tests

## Version 1.0

* initial commit
