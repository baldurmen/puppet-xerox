class xerox::absent {

  exec {
    'remove printer':
      command     => "/usr/bin/xeroxquemgr -removeq ${xerox::printer_name}",
      refreshonly => true,
      before      => [ File['/opt/Xerox'], Package['xeroxprtdrv'] ];
  }

  package {
    'xeroxprtdrv':
      ensure => purged;
  }

  file {
    '/var/cache/apt/archives/xeroxprtdrv.deb':
      ensure => absent,
      notify => Exec['remove printer'];

    '/opt/Xerox':
      ensure => absent,
      force  => true;
  }
}
