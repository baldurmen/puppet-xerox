class xerox::present {

  package {
    [ 'cups',
      'system-config-printer' ]:
        ensure => present;
  }

  # Drivers

  file {
    '/var/cache/apt/archives/xeroxprtdrv.deb':
      ensure  => present,
      source  => $xerox::package_path,
      owner   => 'root',
      group   => 0,
      mode    => '0644',
      require => Package['cups'];
  }

  package {
    'xeroxprtdrv':
      ensure   => installed,
      provider => dpkg,
      source   => '/var/cache/apt/archives/xeroxprtdrv.deb';
  }

  # Configure printer queue

  file {
    '/opt/Xerox/prtsys/settings':
      ensure => present,
      source => $xerox::settings_path,
      owner  => 'root',
      group  => 0,
      mode   => '0644';
  }

  exec {
    'configure printer':
      command => "/usr/bin/xeroxquemgr ${xerox::printer_name} -oid ${xerox::oid} -uri lpd://${xerox::ip}/lp -featuresettings /opt/Xerox/prtsys/settings -uienable -defaultq",
      onlyif  => "/usr/bin/xeroxquemgr ${xerox::printer_name} -duplicate",
      require => [Package['xeroxprtdrv'], File['/opt/Xerox/prtsys/settings']];
  }

  # Web interface

  if $xerox::webui {

    host {
      $xerox::webui_domain:
        ensure => present,
        name   => "xerox.${xerox::webui}.lan",
        ip     => $xerox::ip;
    }
  }
}
