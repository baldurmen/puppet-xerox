class xerox (
  Enum[present, absent] $ensure = present,
  String $printer_name = 'Xerox Printer',
  Boolean $webui = false,
  String $webui_domain = undef,
  String $oid = undef,
  String $ip = undef,
  String $package_path = undef,
  String $settings_path = undef,
) {
  if $xerox::ensure == present {
    include ::xerox::present
  }
  elsif $xerox::ensure == absent {
    include ::xerox::absent
  }
}
