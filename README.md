# xerox

## Overview

This module makes it easy to install and manage Xerox multi-function printers on
client computers running Linux. It has been developed for clients using Debian
Stretch and Puppet 4.x.

## What the xerox module affects

In order to work, this modules needs to install the Xerox printer driver on your
computer. **This driver is not free software**. This driver will be placed in
`/var/cache/apt/archives/xeroxprtdrv.deb`.

It then passes commands to the xerox driver to configure printers. It will thus
create two new printer (Print & Job queue) on your system.

If you set the `webui` parameter to `true`, this module will also modify your
`/etc/hosts` to include the printer's IP address under `webui_domain`.

## Usage

**This modules depends on the Xerox Linux printer drivers to run. It is highly
recommended to read [this blog post][blog] to find said driver and to understand
how it works before installing this puppet module.**

To install this module, copy the git repository inside your Puppet's `modules`
directory.

Once this is done, simply apply the module by including it in your manifests:

    class { 
      'xerox':
        ensure        => present,
        printer_name  => 'Printer',
        webui         => true,
        webui_domain  => 'xerox.myprinter.lan',
        oid           => '1.3.6.1.4.1.253.8.62.1.35.2.3',
        ip            => '192.168.1.123',
        package_path  => "puppet:///files/xerox/xeroxprtdrv_x86_64-5.20.606.3946.deb";
        settings_path => "puppet:///files/xerox/settings";
    }

Or in Hiera:

    xerox::ensure: present
    xerox::printer_name: Printer
    xerox::webui: true
    xerox::webui_domain: xerox.myprinter.lan
    xerox::oid: 1.3.6.1.4.1.253.8.62.1.35.2.3
    xerox::ip: 192.168.1.123
    xerox::package_path: puppet:///files/xerox/xeroxprtdrv_x86_64-5.20.606.3946.deb
    xerox::settings_path: puppet:///files/xerox/settings

[blog]: https://veronneau.org/installing-xerox-printers-on-linux.html

## Parameters

All parameters are required.

### ensure

Data type: Enum

The `ensure` parameter installs or uninstall the printer on client computers. It
accepts two values, `present` or `absent`.

If the parameter is set from `present` to `absent`, the xerox printer, drivers
and all related files installed by this module will be uninstalled.

If you wish to reconfigure an installed printer, the best way to do it is
to set this parameter from `present` to `absent`, to wait for puppet to run on
all your clients and then to set it back to `present` once you made your changes.

Default value: `present`

### printer_name

Data type: String

The `printer_name` parameter lets you choose what the printer's name will be.

Default value: `Xerox Printer`

### webui

Data type: Boolean

The `webui` parameter modifies your `/etc/hosts` to include the printer's IP
address under the `webui_domain` domain name. This makes it easy to users to
access the web interface under a friendly URL.

Default value: `false`

### oid

Data Type: String

The `oid` parameter tells the xerox driver your printer's model. For more
information on how to find your `oid`, please read [this blog post][blog].

Default value: `undef`

### ip

Data Type: String

The `ip` parameter passes your printer's IP address to the xerox driver.

Default value: `undef`

### package_path

Data Type: String

The `package_path` parameter tells this module where to look to find the xerox
driver.

Default value: `undef`

### settings_path

Data Type: String

The `settings_path` parameter tells this module where to look to find the
printer's settings file.

Default value: `undef`

## Limitations

This module has been developed for clients using Debian Stretch and  Puppet 4.x.

## Development

If you  want to contribute to this module, please send a merge request.  If you
find a bug, please submit a bug issue in the tracker.
